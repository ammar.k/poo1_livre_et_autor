<h1>1- Exercice 1</h1>

<h2>Livres et Auteurs</h2>

<?php

//require "Author.php";
//require "Livre.php";

spl_autoload_register(function ($class_name) {
    require $class_name . '.php';
});


$auteur1 = new Auteur("King", "Stephen");

$books1 = new Livre("Shining", "1977", 447, 8.20, $auteur1);
$books2 = new Livre("Doctor Sleep", "2013", 544, 9.90, $auteur1);
$books3 = new Livre("ÇA", "1986", 1138, 9.90, $auteur1);
$books4 = new Livre("Simetierre", "1983", 568, 8.20, $auteur1);
$books5 = new Livre("Misery", "1987", 544, 7.90, $auteur1);

$auteur2 = new Auteur("Orwell", "George");
$book1 = new Livre("1984", "1949", 288, 8.20, $auteur2);
$book2 = new Livre("La ferme des animaux", "1945", 128, 5.90, $auteur2);
$book3 = new Livre("Homage to Catalonia", "1938", 256, 6.90, $auteur2);
$book4 = new Livre("Dans la dèche à Paris et à Londres", "1933", 288, 6.90, $auteur2);



echo $auteur1->displayBiography();
echo "<br>";
echo $auteur2->displayBiography();
echo "<br>";



?>