<?php

class Auteur{

    private string $_nom;
    private string $_prenom;
    private array $_livres;

    public function __construct(string $nom, string $prenom){
        $this->_nom = $nom;
        $this->_prenom = $prenom;
        $this->_livres= [];
    }
    

 
    public function getNom()
    {
        return $this->_nom;
    }


    public function setNom(string $_nom)
    {
        $this->_nom = $_nom;

        return $this;
    }

    public function getPrenom()
    {
        return $this->_prenom;
    }

 
    public function setPrenom(string $_prenom)
    {
        $this->_prenom = $_prenom;

        return $this;
    }

    public function addLivre(Livre $livre){
        $this->_livres[] = $livre; 
    }

    public function __toString()
    {
        return $this->_nom . " " . $this->_prenom. "<br>";
    }
    
    public function displayBiography(){
        $result = "<h2>Biographie de $this->_nom $this->_prenom</h2>";


        foreach($this->_livres as $livre){
            $result .= $livre; //pas besoin de faire un echo car on a déjà un echo dans la fonction __toString
        }
        return $result;
        
}
}



?>