<?php


    
    class Livre {
        private string $_titre;
        private string $_anneeParution;
        private int $_nbPages;
        private float $_prix;
        //private string $_auteur;
        private Auteur $_auteur;
    

    public function __construct(string $titre, string $anneeParution, int $nbPages, float $prix, Auteur $auteur){
        $this->_titre = $titre;
        $this->_anneeParution = $anneeParution;
        $this->_nbPages = $nbPages;
        $this->_prix = $prix;
        $this->_auteur = $auteur;
        $this->_auteur->addLivre($this);//on ajoute le livre à l'auteur

    }


    public function getTitre(){
        return $this->_titre;
    }

    public function getAnneeParution(){
        return $this->_anneeParution;
    }

    public function getNbPages(){
        return $this->_nbPages;
    }

    public function getPrix(){
        return $this->_prix;
    }

    public function getAuteur(){
        return $this->_auteur;
    }

    public function setTitre(string $titre){
        $this->_titre = $titre;
    }

    public function setAnneeParution(string $anneeParution){
        $this->_anneeParution = $anneeParution;
    }

    public function setNbPages(int $nbPages){
        $this->_nbPages = $nbPages;
    }

    public function setPrix(float $prix){
        $this->_prix = $prix;
    }


    public function setAuteur(Auteur $auteur){
        $this->_auteur = $auteur; //on ajoute l'auteur au livre
    }

    public function __toString()
    {
        return "Titre : " . $this->_titre . " Année de parution : " . $this->_anneeParution . " Nombre de pages : "
        . $this->_nbPages . " Prix : " . $this->_prix ."<br>";
    }

}

?>

